 import 'package:flutter/material.dart';
import 'package:flutter_smartlook/flutter_smartlook.dart';

class Home extends StatefulWidget {
   const Home({ Key? key }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}
class _HomeState extends State<Home> {

 final List cor = [
   Colors.green, 
   Colors.blue, 
   Colors.red, 
   Colors.black, 
   Colors.white, 
   Colors.grey, 
   Colors.yellow
   ];
  
 late var randomitem = cor[0];
  
  void mudardecor(){

   randomitem = (cor..shuffle()).first;
 
  } 
  @override 
  void initState() {
    super.initState();

    SetupOptions options =
        (SetupOptionsBuilder('139118c4786cb84efffac3a7ba738a386ef30609') 
              ..Fps = 2
              ..StartNewSession = true)
            .build();

    Smartlook.setupAndStartRecording(options);
    Smartlook.startRecording(); // coloquei aqui para gravar no inicio do app

    Smartlook.setEventTrackingMode(EventTrackingMode.FULL_TRACKING);
    List<EventTrackingMode> eventTrackingModes = [
      EventTrackingMode.FULL_TRACKING,
      EventTrackingMode.IGNORE_USER_INTERACTION
    ];
    Smartlook.setEventTrackingModes(eventTrackingModes);
    Smartlook.registerIntegrationListener( CustomIntegrationListener());
    Smartlook.setUserIdentifier('FlutterLul', {"flutter-usr-prop": "valueX"});
    Smartlook.setGlobalEventProperty("key_", "value_", true);
    Smartlook.setGlobalEventProperties({"A": "B"}, false);
    Smartlook.removeGlobalEventProperty("A");
    Smartlook.removeAllGlobalEventProperties();
    Smartlook.setGlobalEventProperty("flutter_global", "value_", true);
    Smartlook.enableWebviewRecording(true);
    Smartlook.enableWebviewRecording(false);
    Smartlook.enableCrashlytics(true);
    Smartlook.setReferrer("referer", "source");
    Smartlook.getDashboardSessionUrl(true);


  }

  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Teste de gerência de estado',
          style: TextStyle(
            color: Colors.black,
          ),
          ),
          backgroundColor: Colors.white,
        ),
        body: Center(
          child: Column(
            children: [
             const SizedBox(
                height: 100,
              ),
              Container(
               color: randomitem,
               width: 100,
               height: 100,
              ),
              TextButton(
              onPressed: () => setState(() {
                mudardecor();
              }),
              style: TextButton.styleFrom(
                backgroundColor: Colors.black,
              ),
              child: Title(
                color: Colors.white, 
                child: const Text('Clique aqui para mudar de cor'),
                    ),
                  ),
                ],
              ),
          ),
        ),
      );
  }
}


class CustomIntegrationListener implements IntegrationListener {
  void onSessionReady(String? dashboardSessionUrl) {
    print("DashboardUrl:");
    print(dashboardSessionUrl);
  }

  void onVisitorReady(String? dashboardVisitorUrl) {
    print("DashboardVisitorUrl:");
    print(dashboardVisitorUrl);
  }
}
